let data = [
     {
          id: 1,
          card_number: "5602221055053843723",
          card_type: "china-unionpay",
          issue_date: "5/25/2021",
          salt: "x6ZHoS0t9vIU",
          phone: "339-555-5239",
     },
     {
          id: 2,
          card_number: "3547469136425635",
          card_type: "jcb",
          issue_date: "12/18/2021",
          salt: "FVOUIk",
          phone: "847-313-1289",
     },
     {
          id: 3,
          card_number: "5610480363247475108",
          card_type: "china-unionpay",
          issue_date: "5/7/2021",
          salt: "jBQThr",
          phone: "348-326-7873",
     },
     {
          id: 4,
          card_number: "374283660946674",
          card_type: "americanexpress",
          issue_date: "1/13/2021",
          salt: "n25JXsxzYr",
          phone: "599-331-8099",
     },
     {
          id: 5,
          card_number: "67090853951061268",
          card_type: "laser",
          issue_date: "3/18/2021",
          salt: "Yy5rjSJw",
          phone: "850-191-9906",
     },
     {
          id: 6,
          card_number: "560221984712769463",
          card_type: "china-unionpay",
          issue_date: "6/29/2021",
          salt: "VyyrJbUhV60",
          phone: "683-417-5044",
     },
     {
          id: 7,
          card_number: "3589433562357794",
          card_type: "jcb",
          issue_date: "11/16/2021",
          salt: "9M3zon",
          phone: "634-798-7829",
     },
     {
          id: 8,
          card_number: "5602255897698404",
          card_type: "china-unionpay",
          issue_date: "1/1/2021",
          salt: "YIMQMW",
          phone: "228-796-2347",
     },
     {
          id: 9,
          card_number: "3534352248361143",
          card_type: "jcb",
          issue_date: "4/28/2021",
          salt: "zj8NhPuUe4I",
          phone: "228-796-2347",
     },
     {
          id: 10,
          card_number: "4026933464803521",
          card_type: "visa-electron",
          issue_date: "10/1/2021",
          salt: "cAsGiHMFTPU",
          phone: "372-887-5974",
     },
];

let cardIssuedBeforeJune = data.reduce((accumulator, currentValue) => {
     let month = currentValue.issue_date.split("/");
     if (month[0] < 6) {
          accumulator.push(currentValue);
     }
     return accumulator;
}, []);
console.log(cardIssuedBeforeJune);



let cvv = data.map((values) => {
     values.cvv = Math.floor(Math.random() * 999 + 100);
     return values;
});
console.log(cvv);



let validData = cvv.map((value) => {
     value.validity = true;
     return value;
});
console.log(validData);



let invalidity = validData.reduce((accumulator, currentValue) => {
     let issueMonth = currentValue.issue_date.split("/");
     if (issueMonth[0] < 3) {
          currentValue.validity = false;
     }
     accumulator.push(currentValue);

     return accumulator;
}, []);
console.log(invalidity);



let sortedDate = data.sort((value1, value2) => {
     let monthData = value1.issue_date.split("/");
     let dateData = value2.issue_date.split("/");
     if (parseInt(monthData[0]) > parseInt(dateData[0])) {
          return 1;
     } else if (parseInt(monthData[0]) < parseInt(dateData[0])) {
          return -1;
     } else {
          if (parseInt(monthData[1]) > parseInt(dateData[1])) {
               return 1;
          } else {
               return -1;
          }
     }
});
console.log(sortedDate);


let groupedValue = data.reduce((accumulator, currentValue) => {
     let months = currentValue.issue_date.split("/");
     if (accumulator[months[0]]) {
          accumulator = { ...accumulator};
     } else {
          accumulator[months[0]] = currentValue;
     }
     return accumulator;
}, {});
console.log(groupedValue);